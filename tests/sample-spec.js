'use strict';
const puppeteer = require('puppeteer');
const expect = require('chai').expect;
const httpServer = require('http-server');
const percySnapshot = require('@percy/puppeteer');

(async () => {
  const caps = {
    'browser': 'chrome',  // You can choose `chrome`, `edge` or `firefox` in this capability
    'browser_version': 'latest',  // We support v83 and above. You can choose `latest`, `latest-beta`, `latest-1`, `latest-2` and so on, in this capability
    'os': 'os x',
    'os_version': 'big sur',
    'build': 'puppeteer-build-1',
    'name': 'My second Puppeteer test',  // The name of your test and build. See browserstack.com/docs/automate/puppeteer/organize tests for more details
    'browserstack.username': process.env.BROWSERSTACK_USERNAME,
    'browserstack.accessKey': process.env.BROWSERSTACK_ACCESS_KEY
  };
  const browser = await puppeteer.connect({
  browserWSEndpoint:
  `wss://cdp.browserstack.com/puppeteer?caps=${encodeURIComponent(JSON.stringify(caps))}`,  // The BrowserStack CDP endpoint gives you a `browser` instance based on the `caps` that you specified
  });

  /* 
  *  The BrowserStack specific code ends here. Following this line is your test script.
  *  Here, we have a simple script that opens duckduckgo.com, searches for the word BrowserStack and asserts the result.
  */
  const page = await browser.newPage();
  await page.goto('https://www.bstackdemo.com');
  await percySnapshot(page, "gitlab issue");
  await browser.close();  // At the end of each of your tests, you must close the browser so that BrowserStack knows when to end the session.
})();


